# api_design_standard

所有后端的API设计都必须以restful 为标准

RESTful API 设计指南 产考如下：

https://segmentfault.com/a/1190000008697972

https://www.toutiao.com/a6625469857057997315/?tt_from=copy_link&utm_campaign=client_share&timestamp=1542763133&app=news_article&utm_source=copy_link&iid=50653263859&utm_medium=toutiao_ios&group_id=6625469857057997315


API 设计举例， 比如用户注册, 登录，登出的restful aip 怎么设计：

产考：
https://www.v2ex.com/t/118049
一个参考 http://stackoverflow.com/questions/7140074/restfully-design-login-or-register-resources


登入/登出对应的服务端资源应该是session，所以相关api应该如下：

GET /session # 获取会话信息
POST /session # 创建新的会话（登入）
PUT /session # 更新会话信息
DELETE /session # 销毁当前会话（登出）

而注册对应的资源是user，api如下：

GET /user/:id # 获取id用户的信息
POST /user # 创建新的用户（注册）
PUT /user/:id # 更新id用户的信息
DELETE /user/:id # 删除id用户（注销）

